import { Diagnostic } from "vscode-languageserver";
import { RuleDocumentation } from "html-validate";

export interface ExtendedDiagnostic extends Diagnostic {
	documentation?: RuleDocumentation;
}
