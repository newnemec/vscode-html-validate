import { HtmlValidate } from "html-validate";
import { ExtendedDiagnostic } from "./extended-diagnostic";

export interface ApiContext {
	getDocumentDiagnostics(resource: string): Promise<ExtendedDiagnostic[]>;
	getDocumentValidator(resource: string): Promise<HtmlValidate | null>;
	trace(message: string, verbose?: string): void;
}
