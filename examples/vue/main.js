import Vue from "vue";

const app = new Vue({
	el: "#app",
	/* the elements are closed out-of-order and should yield an error */
	template: "<p>Lorem ipsum</i>",
});
app.$mount("#app");
