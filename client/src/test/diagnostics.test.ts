import { expect } from "chai";

/* eslint-disable-next-line import/no-unresolved -- will be resolved when running inside vscode */
import vscode from "vscode";

import { getDocUri, activate } from "./helper";

declare module "vscode" {
	interface Diagnostic {
		data?: unknown;
		hasDiagnosticCode: boolean;
	}
}

describe("vscode-html-validate", () => {
	it("should find errors in html/index.html", async () => {
		const docUri = getDocUri("html/index.html");
		await testDiagnostics(docUri, [
			makeError({
				message: "Element <p> is implicitly closed by adjacent <address>",
				range: toRange(0, 1, 0, 2),
				code: "no-implicit-close",
			}),
			makeError({
				message: "Unexpected close-tag, expected opening tag.",
				range: toRange(2, 1, 2, 3),
				code: "close-order",
			}),
			makeError({
				message: '<button> is missing required "type" attribute',
				range: toRange(4, 1, 4, 7),
				severity: vscode.DiagnosticSeverity.Warning,
				code: "element-required-attributes",
			}),
		]);
	});

	it("should not find errors in text/plaintext.txt", async () => {
		const docUri = getDocUri("text/plaintext.txt");
		await testDiagnostics(docUri, []);
	});

	it("should find errors in vue/main.js", async () => {
		const docUri = getDocUri("vue/main.js");
		await testDiagnostics(docUri, [
			makeError({
				message: "Mismatched close-tag, expected '</p>' but found '</i>'.",
				range: toRange(5, 27, 5, 29),
				code: "close-order",
			}),
		]);
	});
});

function toRange(sLine: number, sChar: number, eLine: number, eChar: number): vscode.Range {
	const start = new vscode.Position(sLine, sChar);
	const end = new vscode.Position(eLine, eChar);
	return new vscode.Range(start, end);
}

function makeError(partial: Partial<vscode.Diagnostic>): vscode.Diagnostic {
	return {
		message: "",
		range: toRange(0, 0, 0, 0),
		severity: vscode.DiagnosticSeverity.Error,
		code: "no-implicit-close",
		data: undefined,
		hasDiagnosticCode: false,
		source: "html-validate",
		...partial,
	};
}

async function testDiagnostics(
	docUri: vscode.Uri,
	expectedDiagnostics: vscode.Diagnostic[]
): Promise<void> {
	await activate(docUri);
	const actualDiagnostics = vscode.languages.getDiagnostics(docUri);
	expect(actualDiagnostics).to.eql(expectedDiagnostics);
}
