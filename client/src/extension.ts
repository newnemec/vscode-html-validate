import path from "path";

import vscode, {
	workspace,
	ExtensionContext,
	window as Window,
} from "vscode"; /* eslint-disable-line import/no-unresolved -- will be resolved when running inside vscode */

import {
	LanguageClient,
	LanguageClientOptions,
	ServerOptions,
	TransportKind,
	RequestType,
} from "vscode-languageclient/node";

import * as commands from "./commands";

interface MinVersionErrorParams {
	min: string;
	cur: string;
}

interface ConfigErrorParams {
	error: string;
}

const MinVersionErrorRequest = new RequestType<MinVersionErrorParams, void, void>(
	"html-validate/min-version-error"
);

const ConfigErrorRequest = new RequestType<ConfigErrorParams, void, void>(
	"html-validate/config-error"
);

let client: LanguageClient;

export function activate(context: ExtensionContext): void {
	const serverModule = context.asAbsolutePath(path.join("server", "out", "server.js"));

	const serverOptions: ServerOptions = {
		run: { module: serverModule, transport: TransportKind.ipc },
		debug: {
			module: serverModule,
			transport: TransportKind.ipc,
			options: { execArgv: ["--nolazy", "--inspect=6009"] },
		},
	};

	const clientOptions: LanguageClientOptions = {
		documentSelector: [{ scheme: "file" }],
		synchronize: {
			fileEvents: [
				workspace.createFileSystemWatcher("**/.htmlvalidate.{js,json}"),
				workspace.createFileSystemWatcher("**/package.json"),

				/** TODO should look at .htmlvalidate.json to determine if any custom
				 * element metadata is present and watch those files in particular, no
				 * matter the filename */
				workspace.createFileSystemWatcher("**/elements.{js,json}"),
			],
		},
	};

	client = new LanguageClient("html-validate", "HTML-Validate", serverOptions, clientOptions);

	client.onReady().then(() => {
		/* show error if the client html-validate version isn't recent enough */
		client.onRequest(MinVersionErrorRequest, handleMinVersionError);

		/* show error if a configuration error was detected */
		client.onRequest(ConfigErrorRequest, handleConfigError);
	});

	client.start();

	/* map commands */
	vscode.commands.registerCommand("html-validate.rule-documentation", commands.ruleDocumentation);
}

function handleMinVersionError(params: MinVersionErrorParams): void {
	client.warn(
		[
			"Failed to load html-validate library.",
			`Minimum required version is ${params.min} but current version is ${params.cur}`,
			"Please install a later version to continue.",
		].join("\n")
	);
	Window.showErrorMessage(`Failed to load html-validate library, version ${params.min} required.`);
	client.outputChannel.show(true);
}

function handleConfigError(params: ConfigErrorParams): void {
	client.warn(
		[
			"HTML-Validate crashed due to a configuration error.",
			`Validation has been disabled for this document until the issue is resolved.`,
			params.error,
		].join("\n")
	);
	Window.showErrorMessage(
		`HTML-Validate crashed due to a configuration error. Disabled for this document.`
	);
	client.outputChannel.show(true);
}

export function deactivate(): Thenable<void> | undefined {
	if (!client) {
		return undefined;
	}
	return client.stop();
}
